<?php
require_once(getabspath("classes/cipherer.php"));




$tdatager_cte = array();
	$tdatager_cte[".truncateText"] = true;
	$tdatager_cte[".NumberOfChars"] = 80;
	$tdatager_cte[".ShortName"] = "ger_cte";
	$tdatager_cte[".OwnerID"] = "";
	$tdatager_cte[".OriginalTable"] = "ger_cte";

//	field labels
$fieldLabelsger_cte = array();
$fieldToolTipsger_cte = array();
$pageTitlesger_cte = array();

if(mlang_getcurrentlang()=="Portuguese(Brazil)")
{
	$fieldLabelsger_cte["Portuguese(Brazil)"] = array();
	$fieldToolTipsger_cte["Portuguese(Brazil)"] = array();
	$pageTitlesger_cte["Portuguese(Brazil)"] = array();
	$fieldLabelsger_cte["Portuguese(Brazil)"]["id_cte"] = "Id Cte";
	$fieldToolTipsger_cte["Portuguese(Brazil)"]["id_cte"] = "";
	if (count($fieldToolTipsger_cte["Portuguese(Brazil)"]))
		$tdatager_cte[".isUseToolTips"] = true;
}
if(mlang_getcurrentlang()=="")
{
	$fieldLabelsger_cte[""] = array();
	$fieldToolTipsger_cte[""] = array();
	$pageTitlesger_cte[""] = array();
	if (count($fieldToolTipsger_cte[""]))
		$tdatager_cte[".isUseToolTips"] = true;
}


	$tdatager_cte[".NCSearch"] = true;



$tdatager_cte[".shortTableName"] = "ger_cte";
$tdatager_cte[".nSecOptions"] = 0;
$tdatager_cte[".recsPerRowList"] = 1;
$tdatager_cte[".recsPerRowPrint"] = 1;
$tdatager_cte[".mainTableOwnerID"] = "";
$tdatager_cte[".moveNext"] = 1;
$tdatager_cte[".entityType"] = 0;

$tdatager_cte[".strOriginalTableName"] = "ger_cte";





$tdatager_cte[".showAddInPopup"] = false;

$tdatager_cte[".showEditInPopup"] = false;

$tdatager_cte[".showViewInPopup"] = false;

//page's base css files names
$popupPagesLayoutNames = array();
$tdatager_cte[".popupPagesLayoutNames"] = $popupPagesLayoutNames;


$tdatager_cte[".fieldsForRegister"] = array();

$tdatager_cte[".listAjax"] = false;

	$tdatager_cte[".audit"] = false;

	$tdatager_cte[".locking"] = false;

$tdatager_cte[".edit"] = true;
$tdatager_cte[".afterEditAction"] = 1;
$tdatager_cte[".closePopupAfterEdit"] = 1;
$tdatager_cte[".afterEditActionDetTable"] = "";

$tdatager_cte[".add"] = true;
$tdatager_cte[".afterAddAction"] = 1;
$tdatager_cte[".closePopupAfterAdd"] = 1;
$tdatager_cte[".afterAddActionDetTable"] = "";

$tdatager_cte[".list"] = true;

$tdatager_cte[".view"] = true;

$tdatager_cte[".import"] = true;

$tdatager_cte[".exportTo"] = true;

$tdatager_cte[".printFriendly"] = true;

$tdatager_cte[".delete"] = true;

$tdatager_cte[".showSimpleSearchOptions"] = false;

// search Saving settings
$tdatager_cte[".searchSaving"] = false;
//

$tdatager_cte[".showSearchPanel"] = true;
		$tdatager_cte[".flexibleSearch"] = true;

if (isMobile())
	$tdatager_cte[".isUseAjaxSuggest"] = false;
else
	$tdatager_cte[".isUseAjaxSuggest"] = true;

$tdatager_cte[".rowHighlite"] = true;



$tdatager_cte[".addPageEvents"] = false;

// use timepicker for search panel
$tdatager_cte[".isUseTimeForSearch"] = false;



$tdatager_cte[".badgeColor"] = "00C2C5";


$tdatager_cte[".allSearchFields"] = array();
$tdatager_cte[".filterFields"] = array();
$tdatager_cte[".requiredSearchFields"] = array();

$tdatager_cte[".allSearchFields"][] = "id_cte";
	

$tdatager_cte[".googleLikeFields"] = array();
$tdatager_cte[".googleLikeFields"][] = "id_cte";


$tdatager_cte[".advSearchFields"] = array();
$tdatager_cte[".advSearchFields"][] = "id_cte";

$tdatager_cte[".tableType"] = "list";

$tdatager_cte[".printerPageOrientation"] = 0;
$tdatager_cte[".nPrinterPageScale"] = 100;

$tdatager_cte[".nPrinterSplitRecords"] = 40;

$tdatager_cte[".nPrinterPDFSplitRecords"] = 40;



$tdatager_cte[".geocodingEnabled"] = false;





$tdatager_cte[".listGridLayout"] = 3;

$tdatager_cte[".isDisplayLoading"] = true;




// view page pdf

// print page pdf


$tdatager_cte[".pageSize"] = 20;

$tdatager_cte[".warnLeavingPages"] = true;



$tstrOrderBy = "";
if(strlen($tstrOrderBy) && strtolower(substr($tstrOrderBy,0,8))!="order by")
	$tstrOrderBy = "order by ".$tstrOrderBy;
$tdatager_cte[".strOrderBy"] = $tstrOrderBy;

$tdatager_cte[".orderindexes"] = array();

$tdatager_cte[".sqlHead"] = "SELECT id_cte";
$tdatager_cte[".sqlFrom"] = "FROM ger_cte";
$tdatager_cte[".sqlWhereExpr"] = "";
$tdatager_cte[".sqlTail"] = "";











//fill array of records per page for list and report without group fields
$arrRPP = array();
$arrRPP[] = 10;
$arrRPP[] = 20;
$arrRPP[] = 30;
$arrRPP[] = 50;
$arrRPP[] = 100;
$arrRPP[] = 500;
$arrRPP[] = -1;
$tdatager_cte[".arrRecsPerPage"] = $arrRPP;

//fill array of groups per page for report with group fields
$arrGPP = array();
$arrGPP[] = 1;
$arrGPP[] = 3;
$arrGPP[] = 5;
$arrGPP[] = 10;
$arrGPP[] = 50;
$arrGPP[] = 100;
$arrGPP[] = -1;
$tdatager_cte[".arrGroupsPerPage"] = $arrGPP;

$tdatager_cte[".highlightSearchResults"] = true;

$tableKeysger_cte = array();
$tableKeysger_cte[] = "id_cte";
$tdatager_cte[".Keys"] = $tableKeysger_cte;

$tdatager_cte[".listFields"] = array();
$tdatager_cte[".listFields"][] = "id_cte";

$tdatager_cte[".hideMobileList"] = array();


$tdatager_cte[".viewFields"] = array();
$tdatager_cte[".viewFields"][] = "id_cte";

$tdatager_cte[".addFields"] = array();

$tdatager_cte[".masterListFields"] = array();
$tdatager_cte[".masterListFields"][] = "id_cte";

$tdatager_cte[".inlineAddFields"] = array();

$tdatager_cte[".editFields"] = array();

$tdatager_cte[".inlineEditFields"] = array();

$tdatager_cte[".exportFields"] = array();
$tdatager_cte[".exportFields"][] = "id_cte";

$tdatager_cte[".importFields"] = array();
$tdatager_cte[".importFields"][] = "id_cte";

$tdatager_cte[".printFields"] = array();
$tdatager_cte[".printFields"][] = "id_cte";

//	id_cte
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 1;
	$fdata["strName"] = "id_cte";
	$fdata["GoodName"] = "id_cte";
	$fdata["ownerTable"] = "ger_cte";
	$fdata["Label"] = GetFieldLabel("ger_cte","id_cte");
	$fdata["FieldType"] = 3;

	
		$fdata["AutoInc"] = true;

	
	
		$fdata["bListPage"] = true;

	
	
	
	
		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "id_cte";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "id_cte";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "number";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
	
			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty");
// the end of search options settings




	$tdatager_cte["id_cte"] = $fdata;


$tables_data["ger_cte"]=&$tdatager_cte;
$field_labels["ger_cte"] = &$fieldLabelsger_cte;
$fieldToolTips["ger_cte"] = &$fieldToolTipsger_cte;
$page_titles["ger_cte"] = &$pageTitlesger_cte;

// -----------------start  prepare master-details data arrays ------------------------------//
// tables which are detail tables for current table (master)
$detailsTablesData["ger_cte"] = array();

// tables which are master tables for current table (detail)
$masterTablesData["ger_cte"] = array();


// -----------------end  prepare master-details data arrays ------------------------------//

require_once(getabspath("classes/sql.php"));










function createSqlQuery_ger_cte()
{
$proto0=array();
$proto0["m_strHead"] = "SELECT";
$proto0["m_strFieldList"] = "id_cte";
$proto0["m_strFrom"] = "FROM ger_cte";
$proto0["m_strWhere"] = "";
$proto0["m_strOrderBy"] = "";
$proto0["m_strTail"] = "";
	
		;
			$proto0["cipherer"] = null;
$proto2=array();
$proto2["m_sql"] = "";
$proto2["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto2["m_column"]=$obj;
$proto2["m_contained"] = array();
$proto2["m_strCase"] = "";
$proto2["m_havingmode"] = false;
$proto2["m_inBrackets"] = false;
$proto2["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto2);

$proto0["m_where"] = $obj;
$proto4=array();
$proto4["m_sql"] = "";
$proto4["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto4["m_column"]=$obj;
$proto4["m_contained"] = array();
$proto4["m_strCase"] = "";
$proto4["m_havingmode"] = false;
$proto4["m_inBrackets"] = false;
$proto4["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto4);

$proto0["m_having"] = $obj;
$proto0["m_fieldlist"] = array();
						$proto6=array();
			$obj = new SQLField(array(
	"m_strName" => "id_cte",
	"m_strTable" => "ger_cte",
	"m_srcTableName" => "ger_cte"
));

$proto6["m_sql"] = "id_cte";
$proto6["m_srcTableName"] = "ger_cte";
$proto6["m_expr"]=$obj;
$proto6["m_alias"] = "";
$obj = new SQLFieldListItem($proto6);

$proto0["m_fieldlist"][]=$obj;
$proto0["m_fromlist"] = array();
												$proto8=array();
$proto8["m_link"] = "SQLL_MAIN";
			$proto9=array();
$proto9["m_strName"] = "ger_cte";
$proto9["m_srcTableName"] = "ger_cte";
$proto9["m_columns"] = array();
$proto9["m_columns"][] = "id_cte";
$obj = new SQLTable($proto9);

$proto8["m_table"] = $obj;
$proto8["m_sql"] = "ger_cte";
$proto8["m_alias"] = "";
$proto8["m_srcTableName"] = "ger_cte";
$proto10=array();
$proto10["m_sql"] = "";
$proto10["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto10["m_column"]=$obj;
$proto10["m_contained"] = array();
$proto10["m_strCase"] = "";
$proto10["m_havingmode"] = false;
$proto10["m_inBrackets"] = false;
$proto10["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto10);

$proto8["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto8);

$proto0["m_fromlist"][]=$obj;
$proto0["m_groupby"] = array();
$proto0["m_orderby"] = array();
$proto0["m_srcTableName"]="ger_cte";		
$obj = new SQLQuery($proto0);

	return $obj;
}
$queryData_ger_cte = createSqlQuery_ger_cte();


	
		;

	

$tdatager_cte[".sqlquery"] = $queryData_ger_cte;

$tableEvents["ger_cte"] = new eventsBase;
$tdatager_cte[".hasEvents"] = false;

?>