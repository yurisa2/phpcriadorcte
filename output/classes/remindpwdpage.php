<?php
class RemindPasswordPage extends RunnerPage
{
	
	function __construct(&$params)
	{
		parent::__construct($params);
		$this->pSet = new ProjectSettings("php_sa2_cte_maker_users", $this->pageType);
		$this->pSetEdit = $this->pSet;
		
		$this->formBricks["header"] = "remindheader";
		$this->formBricks["footer"] = "remindbuttons";
		$this->assignFormFooterAndHeaderBricks( true );	
	}

	/**
	 * Set the connection property
	 */
	protected function setTableConnection()
	{
		global $cman;
		$this->connection = $cman->getForLogin();		
	}	
	protected function assignCipherer()
	{
		$this->cipherer = RunnerCipherer::getForLogin();
	}
	
	/**
	 * Check is captcha exists on current page
	 *
	 * @intellisense
	 */	
	function captchaExists()
	{
		$captchaSettings = GetGlobalData("CaptchaSettings", false);
		return $captchaSettings["isEnabledOnRemind"];
	}

	function getCaptchaId() {
		return "remind";
	}

}
?>