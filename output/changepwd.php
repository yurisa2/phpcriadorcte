<?php
@ini_set("display_errors","1");
@ini_set("display_startup_errors","1");

require_once("include/dbcommon.php");

if(!isLogged() || @$_SESSION["UserID"]=="<Guest>")
{ 
	Security::saveRedirectURL();
	HeaderRedirect("login", "", "message=expired"); 
	return;
}

require_once('include/xtempl.php');
require_once('classes/changepwdpage.php');
require_once(getabspath("classes/cipherer.php"));

$xt = new Xtempl();
$id = postvalue("id") != "" ? postvalue("id") : 1;
$message = "";

$changePwdFields = array('oldpass', 'newpass', 'confirm');

$cipherer = RunnerCipherer::getForLogin();




$layout = new TLayout("changepwd_bootstrap", "OfficeOffice", "MobileOffice");
$layout->version = 3;
	$layout->bootstrapTheme = "cosmo";
$layout->blocks["top"] = array();
$layout->containers["page"] = array();
$layout->container_properties["page"] = array(  );
$layout->containers["page"][] = array("name"=>"wrapper",
	"block"=>"", "substyle"=>1 , "container"=>"page_1" );
$layout->containers["page_1"] = array();
$layout->container_properties["page_1"] = array(  );
$layout->containers["page_1"][] = array("name"=>"wrapper",
	"block"=>"", "substyle"=>1 , "container"=>"panel" );
$layout->containers["panel"] = array();
$layout->container_properties["panel"] = array(  );
$layout->containers["panel"][] = array("name"=>"wrapper",
	"block"=>"", "substyle"=>1 , "container"=>"header" );
$layout->containers["header"] = array();
$layout->container_properties["header"] = array(  );
$layout->containers["header"][] = array("name"=>"changeheader",
	"block"=>"changeheader", "substyle"=>1  );

$layout->skins["header"] = "";


$layout->containers["panel"][] = array("name"=>"wrapper",
	"block"=>"", "substyle"=>1 , "container"=>"body" );
$layout->containers["body"] = array();
$layout->container_properties["body"] = array(  );
$layout->containers["body"][] = array("name"=>"wrapper",
	"block"=>"", "substyle"=>1 , "container"=>"header_1" );
$layout->containers["header_1"] = array();
$layout->container_properties["header_1"] = array(  );
$layout->containers["header_1"][] = array("name"=>"message",
	"block"=>"message_block", "substyle"=>1  );

$layout->skins["header_1"] = "";


$layout->containers["body"][] = array("name"=>"wrapper",
	"block"=>"", "substyle"=>1 , "container"=>"fields" );
$layout->containers["fields"] = array();
$layout->container_properties["fields"] = array(  );
$layout->containers["fields"][] = array("name"=>"changefields",
	"block"=>"", "substyle"=>1  );

$layout->skins["fields"] = "";


$layout->containers["body"][] = array("name"=>"wrapper",
	"block"=>"", "substyle"=>1 , "container"=>"header_2" );
$layout->containers["header_2"] = array();
$layout->container_properties["header_2"] = array(  );
$layout->containers["header_2"][] = array("name"=>"changebuttons",
	"block"=>"changebuttons", "substyle"=>1  );

$layout->skins["header_2"] = "";


$layout->skins["body"] = "";


$layout->skins["panel"] = "";


$layout->skins["page_1"] = "";


$layout->skins["page"] = "";

$layout->blocks["top"][] = "page";
$page_layouts["changepwd"] = $layout;




//array of params for classes
$params = array("pageType" => PAGE_CHANGEPASS, "id" =>$id);
$params['xt'] = &$xt;
$params['tName'] = NOT_TABLE_BASED_TNAME;
$params['templatefile'] = "changepwd.htm";
$params['needSearchClauseObj'] = false;
$pageObject = new ChangePasswordPage($params);
$pageObject->init();

$onFly = postvalue("onFly");

$referer = @$_SERVER["HTTP_REFERER"] != "" 
		&& strpos($_SERVER["HTTP_REFERER"], GetTableLink("changepwd")) != strlen($_SERVER["HTTP_REFERER"]) - strlen(GetTableLink("changepwd"))
		? $_SERVER["HTTP_REFERER"] : ""; 

if(!isset($_SESSION["changepwd_referer"]))
	$_SESSION["changepwd_referer"] = $referer != "" ? $referer : GetTableLink("menu");
else if($referer != "")
	$_SESSION["changepwd_referer"] = $referer;

$auditObj = GetAuditObject();

//	Before Process event
if($globalEvents->exists("BeforeProcessChangePwd"))
	$globalEvents->BeforeProcessChangePwd( $pageObject );

if (@$_POST["btnSubmit"] == "Change")
{	
	$xt->assign( "backlink_attrs", "href=\"". runner_htmlspecialchars( $_SESSION["changepwd_referer"] ) . "\"" );
	
	$filename_values = array();
	$blobfields = array();
	$values = array();
	$strTableName = $cLoginTable;
	foreach($changePwdFields as $fName)
	{
		$fControl = $pageObject->getControl($fName, $id);
		$fControl->readWebValue($values, $blobfields, NULL, NULL, $filename_values);
	}
		
	
	$value = @$_SESSION["UserID"];
	if($cipherer->isFieldEncrypted($cUserNameField))
		$value = $cipherer->MakeDBValue($cUserNameField, $value, "", true);
	else
	{
		if(NeedQuotes($cUserNameFieldType))
			$value = $pageObject->connection->prepareString($value);
		else
			$value = (0 + $value);
	}
	
	$passvalue = $values["newpass"];
	
	if($cipherer->isFieldEncrypted($cPasswordField))
		$passvalue = $cipherer->MakeDBValue($cPasswordField, $passvalue);
	else
	{
		if(NeedQuotes($cPasswordFieldType))
			$passvalue = $pageObject->connection->prepareString($passvalue);
		else
			$passvalue = (0 + $passvalue);
	}
	
	//possible to do through $connection->addTableWrappers( . . .)
	$sWhere = " where ".$pageObject->connection->addFieldWrappers($cUserNameField)."=".$value;
	$strSQL = "select ".$pageObject->connection->addFieldWrappers($cPasswordField);
	$strSQL .= " as ".$pageObject->connection->addFieldWrappers($cPasswordField)
			." from ".$pageObject->connection->addTableWrappers($cLoginTable).$sWhere;
			
	$qResult = $pageObject->connection->query( $strSQL );	
	$row = $cipherer->DecryptFetchedArray( $qResult->fetchAssoc() );
	if($row && ($values['oldpass'] == $row[$cPasswordField]))
	{
		if($pageObject->pwdStrong && !checkpassword($values['newpass']))
		{
			$msg = "";
			$pwdLen = GetGlobalData("pwdLen", 0);
			if($pwdLen)
			{
				$fmt = "A senha deverá conter ao menos %%  caracteres.";
				$fmt = str_replace("%%", "".$pwdLen, $fmt);
				$msg.= "<br>".$fmt;
			}
			$pwdUnique = GetGlobalData("pwdUnique", 0);
			if($pwdUnique)
			{
				$fmt = "A senha deverá conter %% caracteres sem repetição";
				$fmt = str_replace("%%", "".$pwdUnique, $fmt);
				$msg.= "<br>".$fmt;
			}
			$pwdDigits = GetGlobalData("pwdDigits", 0);
			if($pwdDigits)
			{
				$fmt = "A senha deverá conter %% dígitos ou símbolos";
				$fmt = str_replace("%%", "".$pwdDigits, $fmt);
				$msg.= "<br>".$fmt;
			}
			if(GetGlobalData("pwdUpperLower", false))
			{
				$fmt = "A senha deve conter letras maiúculas e minúsculas";
				$msg.= "<br>".$fmt;
			}
			
			if($msg)
				$msg = substr($msg, 4);
			
			$message = $msg;
			$pageObject->jsSettings['tableSettings'][$strTableName]['msg_passwordError'] = $msg;
			$allow_registration = false;
		}
		else
		{
			$retval = true;
			if($globalEvents->exists("BeforeChangePassword"))
				$retval = $globalEvents->BeforeChangePassword($values["oldpass"], $values["newpass"], $pageObject);
			if($retval)
			{
				$strSQL = "update ".$pageObject->connection->addTableWrappers($cLoginTable)
						 ." set ".$pageObject->connection->addFieldWrappers($cPasswordField)."=".$passvalue.$sWhere;
					
				$pageObject->connection->exec( $strSQL );

				if($auditObj)
					$auditObj->LogChPassword();
				if($globalEvents->exists("AfterChangePassword"))
					$globalEvents->AfterChangePassword($values["oldpass"], $values["newpass"], $pageObject);
				
				


$layout = new TLayout("changepwd_success_bootstrap", "OfficeOffice", "MobileOffice");
$layout->version = 3;
	$layout->bootstrapTheme = "cosmo";
$layout->blocks["top"] = array();
$layout->containers["page"] = array();
$layout->container_properties["page"] = array(  );
$layout->containers["page"][] = array("name"=>"wrapper",
	"block"=>"", "substyle"=>1 , "container"=>"page_1" );
$layout->containers["page_1"] = array();
$layout->container_properties["page_1"] = array(  );
$layout->containers["page_1"][] = array("name"=>"wrapper",
	"block"=>"", "substyle"=>1 , "container"=>"panel" );
$layout->containers["panel"] = array();
$layout->container_properties["panel"] = array(  );
$layout->containers["panel"][] = array("name"=>"wrapper",
	"block"=>"", "substyle"=>1 , "container"=>"header" );
$layout->containers["header"] = array();
$layout->container_properties["header"] = array(  );
$layout->containers["header"][] = array("name"=>"changeheader",
	"block"=>"changeheader", "substyle"=>1  );

$layout->skins["header"] = "";


$layout->containers["panel"][] = array("name"=>"wrapper",
	"block"=>"", "substyle"=>1 , "container"=>"body" );
$layout->containers["body"] = array();
$layout->container_properties["body"] = array(  );
$layout->containers["body"][] = array("name"=>"wrapper",
	"block"=>"", "substyle"=>1 , "container"=>"header_1" );
$layout->containers["header_1"] = array();
$layout->container_properties["header_1"] = array(  );
$layout->containers["header_1"][] = array("name"=>"changepwd_message",
	"block"=>"", "substyle"=>1  );

$layout->skins["header_1"] = "";


$layout->containers["body"][] = array("name"=>"wrapper",
	"block"=>"", "substyle"=>1 , "container"=>"header_2" );
$layout->containers["header_2"] = array();
$layout->container_properties["header_2"] = array(  );
$layout->containers["header_2"][] = array("name"=>"changesuccessbutton",
	"block"=>"", "substyle"=>1  );

$layout->skins["header_2"] = "";


$layout->skins["body"] = "";


$layout->skins["panel"] = "";


$layout->skins["page_1"] = "";


$layout->skins["page"] = "";

$layout->blocks["top"][] = "page";
$page_layouts["changepwd_success"] = $layout;



				
				$pageObject->updatePageLayoutAndCSS('', 'success');
				
				$xt->assign("body", true);
				return $pageObject->display(GetTemplateName("", "changepwd_success")); // return $xt->display for .net compatibility
			}
		}
	}
	else
	{
		$message = "Senha inválida";
	}
}
else 
{
	$xt->assign("backlink_attrs", "href=\"". runner_htmlspecialchars( $_SESSION["changepwd_referer"] ) . "\"");
}	
	
if ( $message )
{
	if ( $pageObject->getLayoutVersion() == BOOTSTRAP_LAYOUT )
	{
		$xt->assign("message_class", "alert-danger" );
		$xt->assign("message", $message);
	}
	else
	{
		$xt->assign("message", "<div class='message rnr-error'>".$message."</div>");
	}
	
	$xt->assign("message_block", true);
}

foreach($changePwdFields as $fName)
{
	$parameters = array();
	$parameters["id"] = $id;
	$parameters["mode"] = "add";
	$parameters["field"] = $fName;
	$parameters["format"] = "Password";
	$parameters["pageObj"] = $pageObject;
	$parameters["suggest"] = true;
	$parameters["validate"] = Array('basicValidate' => Array('IsRequired')); 
	
	$parameters["extraParams"] = array();
	$parameters["extraParams"]["getConrirmFieldCtrl"] = true;
			
	
	$controls = array('controls'=>array());	
	$controls["controls"]['id'] = $id;
	$controls["controls"]['mode'] = "add";
	$controls["controls"]['ctrlInd'] = 0;
	$controls["controls"]['fieldName'] = $fName;
	$controls["controls"]['suggest'] = $parameters["suggest"];
	
	$xt->assign_function( $fName."_editcontrol", "xt_buildeditcontrol", $parameters );
	$xt->assign($fName."_label", true);
	if ( $pageObject->getLayoutVersion() == BOOTSTRAP_LAYOUT )
	{
		$xt->assign("labelfor_" . goodFieldName($fName), "value_".$fName."_".$id);
	}		
	
	if(isEnableSection508())
		$xt->assign_section($fName."_label", "<label for=\"value_".$fName."_".$id."\">", "</label>");
	
	$xt->assign($fName."_block", true);
			
	$pageObject->fillControlsMap($controls);
}

$xt->assign("submit_attrs", "id=\"saveButton".$id."\"");
$xt->assign("id", $id);

$pageObject->body["begin"] .= GetBaseScriptsForPage(false);
$pageObject->addCommonJs();
$pageObject->fillSetCntrlMaps();
$pageObject->addButtonHandlers();

$pageObject->body['end'] = XTempl::create_method_assignment( "assignBodyEnd", $pageObject );

$xt->assignbyref("body", $pageObject->body);

if($globalEvents->exists("BeforeShowChangePwd"))
	$globalEvents->BeforeShowChangePwd($xt,$pageObject->templatefile, $pageObject);

$pageObject->display($pageObject->templatefile);
?>