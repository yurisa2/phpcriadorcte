<?php
@ini_set("display_errors","1");
@ini_set("display_startup_errors","1");

require_once("include/dbcommon.php");
require_once('classes/remindpwdpage.php');
require_once('include/xtempl.php');
require_once(getabspath("classes/cipherer.php"));

$reminded = false;
$sentMailResults = array();

$cipherer = RunnerCipherer::getForLogin();

$xt = new Xtempl();
$sessPrefix = 'remind';
$id = postvalue("id")!=="" ? postvalue("id") : 1;
$cEmailField = "email";




$layout = new TLayout("remind_bootstrap", "OfficeOffice", "MobileOffice");
$layout->version = 3;
	$layout->bootstrapTheme = "cosmo";
$layout->blocks["top"] = array();
$layout->containers["page"] = array();
$layout->container_properties["page"] = array(  );
$layout->containers["page"][] = array("name"=>"wrapper",
	"block"=>"", "substyle"=>1 , "container"=>"page_1" );
$layout->containers["page_1"] = array();
$layout->container_properties["page_1"] = array(  );
$layout->containers["page_1"][] = array("name"=>"wrapper",
	"block"=>"", "substyle"=>1 , "container"=>"panel" );
$layout->containers["panel"] = array();
$layout->container_properties["panel"] = array(  );
$layout->containers["panel"][] = array("name"=>"wrapper",
	"block"=>"", "substyle"=>1 , "container"=>"header" );
$layout->containers["header"] = array();
$layout->container_properties["header"] = array(  );
$layout->containers["header"][] = array("name"=>"remindheader",
	"block"=>"remindheader", "substyle"=>1  );

$layout->skins["header"] = "";


$layout->containers["panel"][] = array("name"=>"wrapper",
	"block"=>"", "substyle"=>1 , "container"=>"body" );
$layout->containers["body"] = array();
$layout->container_properties["body"] = array(  );
$layout->containers["body"][] = array("name"=>"wrapper",
	"block"=>"", "substyle"=>1 , "container"=>"header_1" );
$layout->containers["header_1"] = array();
$layout->container_properties["header_1"] = array(  );
$layout->containers["header_1"][] = array("name"=>"message",
	"block"=>"message_block", "substyle"=>1  );

$layout->skins["header_1"] = "";


$layout->containers["body"][] = array("name"=>"wrapper",
	"block"=>"", "substyle"=>1 , "container"=>"fields" );
$layout->containers["fields"] = array();
$layout->container_properties["fields"] = array(  );
$layout->containers["fields"][] = array("name"=>"remindfields",
	"block"=>"", "substyle"=>1  );

$layout->skins["fields"] = "";


$layout->containers["body"][] = array("name"=>"wrapper",
	"block"=>"", "substyle"=>1 , "container"=>"header_2" );
$layout->containers["header_2"] = array();
$layout->container_properties["header_2"] = array(  );
$layout->containers["header_2"][] = array("name"=>"remindbuttons",
	"block"=>"remindbuttons", "substyle"=>1  );

$layout->skins["header_2"] = "";


$layout->skins["body"] = "";


$layout->skins["panel"] = "";


$layout->skins["page_1"] = "";


$layout->skins["page"] = "";

$layout->blocks["top"][] = "page";
$page_layouts["remind"] = $layout;




$onFly = postvalue("onFly");
if($onFly == 2) 
    $id = 1;

//array of params for classes
$params = array("id" =>$id, "pageType" => PAGE_REMIND);
$params['xt'] = &$xt;
$params["tName"]= NOT_TABLE_BASED_TNAME;
$params["templatefile"] = "remind.htm";
$params["needSearchClauseObj"] = false;
$params["captchaValue"] = postvalue("value_captcha_" . $id);

$pageObject = new RemindPasswordPage($params);
$pageObject->init();

$xt->assign("closewindow_attrs", 'style="display:none" id="closeWindowRemind"');

$strUsernameEmail="";
$strEmail="";
$strMessage="";

if (@$_POST["btnSubmit"] == "Remind")
{
	//	Before Process event
	if($globalEvents->exists("BeforeProcessRemind"))
		$globalEvents->BeforeProcessRemind( $pageObject );
	
	$strUsernameEmail = postvalue("username_email");
		
	if( $pageObject->checkCaptcha() )
	{
		$tosearch=false;
		
		$value=$strUsernameEmail;
		if((string)$value!="")
			$tosearch=true;
			
		if($cipherer->isFieldEncrypted($cUserNameField))
			$value = $cipherer->MakeDBValue($cUserNameField, $value, "", true);
		else
		{
			if(NeedQuotes($cUserNameFieldType))
				$value = $pageObject->connection->prepareString($value);
			else
				$value=(0+$value);
		}
		 
		$fullcUserNameField = $pageObject->connection->addTableWrappers( "php_sa2_cte_maker_users" ).".".$pageObject->connection->addFieldWrappers( $cUserNameField );
		$sWhere="(".$fullcUserNameField."=".$value;
		
		$value = $strUsernameEmail;
		if($cipherer->isFieldEncrypted($cEmailField))
			$value = $cipherer->MakeDBValue($cEmailField, $value, "", true);
		else
		{
			if(NeedQuotes($cEmailFieldType))
				$value = $pageObject->connection->prepareString($value);
			else
				$value=(0+$value);
		}
		$fullcEmailField = $pageObject->connection->addTableWrappers( "php_sa2_cte_maker_users" ).".".$pageObject->connection->addFieldWrappers( $cEmailField );
		$sWhere.= " or ".$fullcEmailField."=".$value.")";
		$fullcPasswordField = $pageObject->connection->addTableWrappers( "php_sa2_cte_maker_users" ).".".$pageObject->connection->addFieldWrappers( $cPasswordField );
		$selectClause = "select ".$fullcUserNameField
			." as ".$pageObject->connection->addFieldWrappers($cUserNameField)
			.",".$fullcPasswordField." as ".$pageObject->connection->addFieldWrappers($cPasswordField);
		
		// prevent aliases mixing
		if( $cUserNameField != $cEmailField )
			$selectClause.= ",".$fullcEmailField." as ".$pageObject->connection->addFieldWrappers($cEmailField);

		
		$strSQL = $selectClause." from ".$pageObject->connection->addTableWrappers("php_sa2_cte_maker_users")." where ".$sWhere;
		
		$data = $cipherer->DecryptFetchedArray( $pageObject->connection->query( $strSQL )->fetchAssoc() );
		if($data)
		{
			$password=$data[$cPasswordField];
			$strUsername = $data[$cUserNameField];
			$strUserEmail = $data[$cEmailField];
		}
	
		if($tosearch && $globalEvents->exists("BeforeRemindPassword"))
			$tosearch = $globalEvents->BeforeRemindPassword($strUsername,$strUserEmail, $pageObject);
		
		if($tosearch)
		{
			if($data)
			{
		
	
				$url = GetSiteUrl();
				$url.= $_SERVER["SCRIPT_NAME"];
				$url2 = str_replace("remind.","login.",$url)."?username=".rawurlencode($strUsername);
				$message = "";
							


$layout = new TLayout("remind_success_bootstrap", "OfficeOffice", "MobileOffice");
$layout->version = 3;
	$layout->bootstrapTheme = "cosmo";
$layout->blocks["top"] = array();
$layout->containers["page"] = array();
$layout->container_properties["page"] = array(  );
$layout->containers["page"][] = array("name"=>"wrapper",
	"block"=>"", "substyle"=>1 , "container"=>"page_1" );
$layout->containers["page_1"] = array();
$layout->container_properties["page_1"] = array(  );
$layout->containers["page_1"][] = array("name"=>"wrapper",
	"block"=>"", "substyle"=>1 , "container"=>"panel" );
$layout->containers["panel"] = array();
$layout->container_properties["panel"] = array(  );
$layout->containers["panel"][] = array("name"=>"wrapper",
	"block"=>"", "substyle"=>1 , "container"=>"header" );
$layout->containers["header"] = array();
$layout->container_properties["header"] = array(  );
$layout->containers["header"][] = array("name"=>"remindheader",
	"block"=>"remindheader", "substyle"=>1  );

$layout->skins["header"] = "";


$layout->containers["panel"][] = array("name"=>"wrapper",
	"block"=>"", "substyle"=>1 , "container"=>"body" );
$layout->containers["body"] = array();
$layout->container_properties["body"] = array(  );
$layout->containers["body"][] = array("name"=>"wrapper",
	"block"=>"", "substyle"=>1 , "container"=>"fields" );
$layout->containers["fields"] = array();
$layout->container_properties["fields"] = array(  );
$layout->containers["fields"][] = array("name"=>"weresent_message",
	"block"=>"", "substyle"=>1  );

$layout->skins["fields"] = "";


$layout->containers["body"][] = array("name"=>"wrapper",
	"block"=>"", "substyle"=>1 , "container"=>"header_1" );
$layout->containers["header_1"] = array();
$layout->container_properties["header_1"] = array(  );
$layout->containers["header_1"][] = array("name"=>"remindsucbuttons",
	"block"=>"", "substyle"=>1  );

$layout->skins["header_1"] = "";


$layout->skins["body"] = "";


$layout->skins["panel"] = "";


$layout->skins["page_1"] = "";


$layout->skins["page"] = "";

$layout->blocks["top"][] = "page";
$page_layouts["remind_success"] = $layout;



				$message.= "Lembrete de Senha"."\r\n";
				$message.= "Você pediu para lembrar seu nome de usuário e sua senha em"." ".$url2."\r\n";
				$message.= "Nome do Usuário".": ".$strUsername."\r\n";
				$message.= "Senha".": ".$password."\r\n";
				$sentMailResults = runner_mail(array('to' => $strUserEmail, 'subject' => "Lembrete de Senha", 'body' => $message));
				if($sentMailResults['mailed'])
				{
					$pageObject->updatePageLayoutAndCSS('', 'success');
					
					$reminded = true;
					if($globalEvents->exists("AfterRemindPassword"))
						$globalEvents->AfterRemindPassword($strUsername,$strUserEmail,$pageObject);
					
					/*$loginlink_attrs = "href=\"".GetTableLink("login");
					//$loginlink_attrs.="?username=".rawurlencode($strUsernameEmail);
					$loginlink_attrs.="?username=".rawurlencode($strUsername);
					$loginlink_attrs.="\"";
					$loginlink_attrs.=' id="ProceedToLogin"';
					
					$xt->assign("loginlink_attrs",$loginlink_attrs);*/
					$xt->assign("body",true);
					if (postvalue("onFly") != 2)
						return $pageObject->display(GetTemplateName("", "remind_success")); // return $pageObject->display for .net compatibility
				}	
			}
		}
		
		if(!$reminded)
		{
			if($sentMailResults['message'])
				$strMessage = $sentMailResults['message'];
			else
				$strMessage = "Usuário"." ".$strUsernameEmail." "."não é registrado";
		}

	}
	else
	{
		$strMessage = "Código de segurança inválido";
	}
}

// submit on popup page
if (postvalue("onFly") == 2)
{
	if(@$strMessage) {
		$returnJSON['message'] = $strMessage;
	}
	if ( !$pageObject->checkCaptcha() ) {
		$returnJSON['message'] = "Código de segurança inválido";
	}
	if ($reminded) {
		$pageObject->templatefile = GetTemplateName("", "remind_success");
		$xt->assign("id",$id);
		$xt->assign("footer",false);
		$xt->assign("header",false);
		$xt->assign("body",true);
		$pageObject->displayAJAX($pageObject->templatefile, $id+1);
		exit();
	}
	echo printJSON($returnJSON);
	exit();
}

if( $pageObject->captchaExists() )
{
	$pageObject->displayCaptcha();
}

$pageObject->addCommonJs();
$pageObject->fillSetCntrlMaps();



$pageObject->addButtonHandlers();

$xt->assign("submit_attrs","onclick='document.forms.form1.submit();return false;'");

$xt->assign("username_email_label",true);
$is508=isEnableSection508();
if($is508)
{
	$xt->assign_section("username_email_label","<label for=\"username_email\">","</label>");
}
$xt->assign("username_email_attrs",($is508==true ? "id=\"username_email\" " : "")."value=\"".runner_htmlspecialchars($strUsernameEmail)."\"");

if(@$strMessage)
{
	if ( $pageObject->getLayoutVersion() == BOOTSTRAP_LAYOUT )
	{
		$xt->assign("message_class", "alert-danger" );
		$xt->assign("message", $strMessage);
	}
	else 
	{
		$xt->assign("message","<div class='message rnr-error'>".$strMessage."</div>");
	}
	
	$xt->assign("message_block",true);
}

if (postvalue("onFly") != 1)
{
	$pageObject->body["end"] .= "<script>";
	$pageObject->body['end'] .= "window.controlsMap = ".my_json_encode($pageObject->controlsHTMLMap).";";
	$pageObject->body['end'] .= "window.viewControlsMap = ".my_json_encode($pageObject->viewControlsHTMLMap).";";
	$pageObject->body['end'] .= "window.settings = ".my_json_encode($pageObject->jsSettings).";</script>";
		$pageObject->body['end'] .= "<script language=\"JavaScript\" src=\"".GetRootPathForResources("include/runnerJS/RunnerAll.js")."\"></script>\r\n";
	$pageObject->body["end"] .= "<script>".$pageObject->PrepareJS()."</script>";

	$pageObject->body["begin"] .= GetBaseScriptsForPage(false);

	$pageObject->body["begin"] .="<script language = JavaScript>
	function OnKeyDown()
	{
		e = window.event;
		if (e.keyCode == 13)
		{
			e.cancel = true;
			document.forms[0].submit();
		}	
	}
	</script>
	<form method=post action=\"".GetTableLink("remind")."\" id=form1 name=form1>
	<input type=hidden name=btnSubmit value=\"Remind\">
	<input type=\"Hidden\" name=\"searchby\" value=\"".$strSearchBy."\">";
	$pageObject->body["end"] .= "</form>
		<script language=\"JavaScript\">
		".$pageObject->PrepareJS()."
		</script>";

	$xt->assignbyref("body",$pageObject->body);
}

if($globalEvents->exists("BeforeShowRemindPwd"))
	$globalEvents->BeforeShowRemindPwd($xt,$pageObject->templatefile, $pageObject);

// load popup page
if (postvalue("onFly") == 1)
{
	$xt->assign("message_block",true);
	$xt->assign("message","<div id='remind_message' class='message rnr-error'></div>");
	$xt->displayBrickHidden("message");
	$xt->assign("id",$id);
	$xt->assign("footer",false);
	$xt->assign("header",false);
	$xt->assign("body",$pageObject->body);	
	$xt->assign("backlink_attrs", 'style="display:none"');
	$xt->assign("submit_attrs","id='submitRemind'");
	
	$pageObject->displayAJAX($pageObject->templatefile, $id+1);
	exit();
}

$pageObject->display($pageObject->templatefile);
